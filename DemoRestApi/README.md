# GitRestApi

The GitRestApi upload default information from Github rest api ('fullName', 'description', 'clone url', 'stars', 'createdAt') to json.
Input parms: owner, repositories-owner

# Non functional requirements

serve 20 requests per second (application should not have obvious scaling bottlenecks)  
end-to-end tests that can be run easily  
ready to deploy to production  

#Change in config.ini
If you want to add extra attributes (to search in github rest api). Append to config.ini file in last line:
e.g.:

a - oryginal attribute from github rest api (search a attribute in github rest api)
b - Your own name for attribute (display b name in return json)

a = b


#Requirements:
- Python3.6.3
- aiohttp

#Qucik start:
Run program in terminal
cd ~/gitrestapi
python3 git_rest_api.py

Open new terminal
curl http://127.0.0.1:8080/repositories/{owner}/{repositories-owner}

