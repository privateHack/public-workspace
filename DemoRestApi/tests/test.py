from aiohttp import web, ClientSession
from github_rest_api import get_config_attr_from_rest_api
import asyncio, pytest
import json, os, sys
import datetime

#Iso format 8601 for datetime
iso_format = '%Y-%m-%dT%H:%M:%SZ'
#value of number clients (only for tests)
number_clients = 20
'''
Exception for limit request from github rest api
'''
class ErrorLimit(Exception):
    pass

async def simulate_git_rest_api_for_tests(request):
    '''
    Asyncio coroutine fake gtihub server. Input request from client via
    rest api. Output json interface
    '''
    path = os.path.join(
                        os.path.dirname(os.path.abspath(__file__)),
                        'go-cloud.json'
                        )
    with open(path) as json_file:
        resp = json.load(json_file)
        return web.Response(text=json.dumps(resp), status=200)

async def update_dict_json_attr(request,response):
    '''
    Asyncio coroutine for search attr from config.ini
    in response Github rest api
    '''
    #create new dict for upload attr from response rest api
    final_dict = {}
    #update global dict base on response github
    for k, v in request.app['config'].items():
        #if str 'msg' available in response raise error with limit request
        if 'message' in list(response.keys()):
            final_dict = response['message']
            raise ErrorLimit(str(final_dict))
        #for date attr parse to iso format
        elif 'created_at' == k:
            final_dict[v] =\
                    str(datetime.datetime.strptime(response[k], iso_format))
        else:
            try:
                final_dict[v] = response[k]
            except Exception as e:
                final_dict[v] = 'N/A in response'
    return final_dict

async def get_client_session(request, url):
    '''
    Asyncio coroutine for get data from api. Additional parse
    output from json to reqested attr
    '''
    async with ClientSession() as session:
        async with session.get(url) as resp:
            ###Change this line only for tests#######
            response = await resp.read()
            response = json.loads(response.decode())
            #########################################
            #At max limit requests cache msg from rest api github
            try:
                response = await update_dict_json_attr(request, response)
            except Exception as e:
                print(e)
                raise ErrorLimit(str(e))
    return response

async def handle(request):
    '''
    Asyncio coroutine: format data to url api github (with args) for input.
    On the output function  get_client_session() return parsed
    data dump to json interface.
    '''
    #url to virtual server github. Only for tests
    url = 'http://127.0.0.1:8080/test_json/{}/{}'.format(
                                request.match_info['owner'],
                                request.match_info['repository']
                                )#input url link with paramter
    #Catch any exception from  request to api github, at error-
    #500 status response
    try:
        status = await get_client_session(request, url)
        return web.Response(text=json.dumps(status), status=200)
    except Exception as e:
        print(e)
        status = {'status':False, 'error':str(e)}
        return web.Response(text=json.dumps(status), status=500)

################################Start Tests#####################################
@pytest.fixture
def cli(loop, aiohttp_client):
    app = web.Application()
    app['config'] = get_config_attr_from_rest_api(app)
    #create local test server
    app.router.add_route(
                        'GET',
                        '/test_json/zombiezen/go-cloud',
                        simulate_git_rest_api_for_tests
                        )
    app.router.add_route('GET', '/repositories/{owner}/{repository}',
                                            handle)
    return loop.run_until_complete(aiohttp_client(
                                                app,
                                                server_kwargs={'port': 8080})
                                                )

async def test_rest_api_basic(cli):
    '''Get json for example user, repo: zombiezen, go-cloud'''
    resp = await cli.get('/repositories/zombiezen/go-cloud')
    assert resp.status == 200
    #check corrctens fullname
    text = await resp.text()
    assert 'zombiezen/go-cloud' in text

async def test_rest_api_without_one_input(cli):
    '''Get json for exmaple user with not correctly parametrs to endpoint'''
    resp = await cli.get('/repositories/zombiezen')
    assert resp.status == 404

async def test_rest_api_clients(cli):
    '''Simulate 20 req clients'''
    resp = await cli.get('/repositories/zombiezen/go-cloud')
    await run(number_clients)

async def fetch(url, session):
    async with session.get(url) as response:
        assert response.status == 200

async def bound_fetch(semaphore, url, session):
    # Getter function with semaphore
    async with semaphore:
        await fetch(url, session)

async def run(r):
    url = "http://localhost:8080/repositories/zombiezen/go-cloud"
    # create instance of Semaphore (Semaphore value 3 only for tests)
    semaphore = asyncio.Semaphore(3)
    # Create client session that will ensure we dont open new connection
    # per each request.
    async with ClientSession() as session:
        #list comprehension tasks for await to response
        await asyncio.gather(
                            *[
                            asyncio.ensure_future(bound_fetch(
                                                    semaphore, url, session
                                                    ))\
                            for _ in range(r)
                            ]
                        )
