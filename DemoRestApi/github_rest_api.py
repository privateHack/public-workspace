from aiohttp import web, ClientSession
import asyncio
import json, os, sys
import configparser
import datetime

#Iso format 8601 for datetime
iso_format = '%Y-%m-%dT%H:%M:%SZ'

class ErrorLimit(Exception):
    pass

def get_config_attr_from_rest_api(app, path='config.ini'):
    '''
    Function to get data from config.ini and next update to
    global dict json_attr
    '''
    path = os.path.join(
                    os.path.dirname(os.path.abspath(__file__)),
                    path
                    )
    config = configparser.ConfigParser()
    config.sections()
    config.read(path)
    return config['DEFAULT']

async def update_dict_json_attr(request,response):
    '''
    Asyncio coroutine for search attr from config.ini in response Github
    rest api
    '''
    #create new dict for upload attr from response rest api
    final_dict = {}
    #update global dict base on response github
    for k, v in request.app['config'].items():
        #if str 'msg' available in response raise error with limit request
        if 'message' in list(response.keys()):
            final_dict = response
            raise ErrorLimit(str(final_dict))
        #for date attr parse to iso format
        elif 'created_at' == k:
            final_dict[v] =\
                    str(datetime.datetime.strptime(response[k], iso_format))
        else:
            try:
                final_dict[v] = response[k]
            except Exception as e:
                final_dict[v] = 'N/A in response'
    return final_dict

async def get_client_session(request,url):
    '''
    Asyncio coroutine for get data from api. Additional parse
    output from json to reqested attr
    '''
    async with ClientSession() as session:
        async with session.get(url) as resp:
            #for production
            response = await resp.json()
            #At max limit requests cache msg from rest api github
            try:
                response = await update_dict_json_attr(request,response)
            except Exception as e:
                response = response['message']
                raise ErrorLimit(str(response))
    return response

async def handle(request):
    '''
    Asyncio coroutine: format data to url api github (with args) for input.
    On the output function  get_client_session() return parsed
    data dump to json interface.
    '''
    #link for production data rest api
    url = 'https://api.github.com/repos/{}/{}'.format(
                                request.match_info['owner'],
                                request.match_info['repository']
                                )#input url link with paramters

    #Catch any exception from  request to api github,
    #at error- 500 status response
    try:
        status = await get_client_session(request, url)
        return web.Response(text=json.dumps(status), status=200)
    except Exception as e:
        status = {'status':False, 'error':str(e)}
        return web.Response(text=json.dumps(status), status=500)

async def web_app_git_hub_rest_api():
    #build instance
    app = web.Application()
    #Get config data
    app['config'] =  get_config_attr_from_rest_api(app)
    #request endpoint for api github
    app.router.add_route('GET', '/repositories/{owner}/{repository}', handle)
    return app
