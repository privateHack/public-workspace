from channels import route
from jobs.consumers import Connection

connect = Connection

channel_routing = [
    # Wire up websocket channels to our consumers:
    route("websocket.connect", connect.ws_connect),
    route("websocket.receive", connect.ws_receive),
    route("websocket.disconnect", connect.ws_disconnect),
]
