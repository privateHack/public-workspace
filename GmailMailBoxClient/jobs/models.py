from django.db import models
from django.utils import timezone


class MailBox(models.Model):
    header = models.CharField(max_length=255)
    body = models.TextField()
    date = models.DateTimeField(default=timezone.now)
    _from = models.CharField(max_length=100)
    id_mail = models.CharField(max_length=255)
    celery_id = models.CharField(max_length=255, null=True, blank=True)
