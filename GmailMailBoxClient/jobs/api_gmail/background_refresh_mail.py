from __future__ import print_function
import httplib2
import os
import threading
import time, sys

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from apiclient import errors
import base64
import email
import inspect

from jobs.models import GmailMailBox as gmb
from jobs.views import *
from django.utils import timezone
from django.shortcuts import render_to_response, render
from django.http import JsonResponse

from django.contrib.auth import views as auth_views
from jobs import views as core_views
from django.contrib.auth.mixins import LoginRequiredMixin

try:
    import argparse
    flags = tools.argparser.parse_args([])
except ImportError:
    flags = None




class ThreadingExample(LoginRequiredMixin):
    SCOPES = 'https://www.googleapis.com/auth/gmail.readonly'
    CLIENT_SECRET_FILE = '/home/jarek/Pulpit/Projects/Django/mysite/global_app/api_gmail/backgorund_secret_key/client_secret_gmail.json'
    APPLICATION_NAME = 'Gmail API Python Quickstart'

    def __init__(self, interval=10):
        self.interval = interval

    def run(self):
        credentials = self.get_credentials()
        http = credentials.authorize(httplib2.Http())
        service = discovery.build('gmail', 'v1', http=http)
        user_id = 'me'
        while True:
            self.ListMessagesWithLabels(service=service, user_id=user_id, label_ids=['CATEGORY_PERSONAL'])
            time.sleep(3)

        #time.sleep(self.interval)

    def post_mail_to_db(self, _from, header, body):
        if not gmb.objects.filter(header=header):
            print('update db mail')
            gmb(header=header, body=body, date=timezone.now(), _from=_from).save()
            mails = gmb.objects.all()


    def get_credentials(self):
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,'gmail-python-quickstart.json')

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            flow.user_agent = APPLICATION_NAME
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else:
                credentials = tools.run(flow, store)
            print('Storing credentials to ' + credential_path)
        return credentials

    def GetMessage(self, service, user_id, msg_id):
        try:
            message = service.users().messages().get(userId=user_id, id=msg_id, format='full').execute()


            #print( 'Message snippet: %s' % message['snippet'])
            for x in message['payload']['headers']:
                for k, v in x.items():
                    if v == 'Subject':
                        subject = (x['value'])
                    elif v == 'From':
                        _from = (x['value'])

            body = message['snippet']

            self.post_mail_to_db(_from=_from, header=subject, body=body)


            #return _from, subject, body
        except (errors.HttpError, error):
            print('An error occurred: %s' % error)

    def ListMessagesWithLabels(self, service, user_id, label_ids):
        responses = service.users().messages().list(userId=user_id, labelIds=label_ids, maxResults=1).execute()
        messages = []

        return [self.GetMessage(service=service, user_id=user_id, msg_id=response['id']) for response in responses['messages'] if 'messages' in responses]
