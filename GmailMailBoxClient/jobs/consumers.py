import json, os, gc
import logging, time, sys
from channels import Channel
from channels.sessions import channel_session
from .tasks import sec3
from example.celery import app
from jobs.models import MailBox as gmb
from channels.auth import channel_session_user_from_http, channel_session_user
from channels import Group
from celery.task.control import revoke

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
import inspect
from celery import result
from functools import lru_cache
from multiprocessing import Process, Manager

import threading, pickle

log = logging.getLogger(__name__)
i = app.control.inspect()

flag = False
id_task = None

clients = 0
id_client = 0

t = None

rc_dict = dict()

def write_rc_channel(data):
    with open("/tmp/data_rc", "wb") as myFile:
        pickle.dump(data, myFile)

def create_daemon_cilents():
    global clients, id_task
    while True:
        print(clients)
        print(id_task)
        time.sleep(5)
        if not clients:
            print('break')
            result.AsyncResult.revoke(id_task, terminate=True)
            id_task = None
            break

def start_deamon():
    t = threading.Thread(target=create_daemon_cilents, args=())
    t.deamon = True
    t.start()


class Singleton(type):
    def __init__(cls, name, bases, attrs, **kwargs):
        super().__init__(name, bases, attrs)
        cls._instance = None

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__call__(*args, **kwargs)
        return cls._instance


class StartTask(object):
    def __init__(self, message):
        self.message = message

    def start(self, data, reply_channel):
        try:
            result = sec3.delay(data, reply_channel)
        except Exception as e:
            print(e)
        return result

    def __del__(self):
      class_name = self.__class__.__name__

class ListenTask(object):
    def listen(self, message, data, reply_channel, flag):
        global id_task
        self.flag = flag
        self.flag = True

        try:
            #if not list(i.active()['celery@jarek-VirtualBox']):
            if id_task == None:
                start_deamon()
                self.s1 = StartTask(message)
                    #while self.flag:
                    #if not list(i.active()['celery@jarek-VirtualBox']):
                print('start task')
                id_task = self.s1.start(data, reply_channel)
            #        time.sleep(0.1)
        except TypeError:
            pass

    def __del__(self):
      class_name = self.__class__.__name__



class Connection():
    global l1


    @channel_session_user
    def ws_connect(message):
        global id_client, clients
        print('connected id: ', id_client)
        clients +=1

        print('actual connected clients: ', clients)
        message.reply_channel.send({
            "text": json.dumps({
                "action": "start_task_1",
            })
        })
        #d[clients] = message.reply_channel.name
        rc_dict[clients]=message.reply_channel.name
        write_rc_channel(data=rc_dict)

        q = gmb.objects.order_by("-_from")
        if len(q) > 0:
            for x in q:
                message.reply_channel.send({
                        "text": json.dumps({
                            "header": x.header,
                            "body": x.body,
                            "_from": x._from,
                            "action":'update_mail',
                            "caluse":'only_once',
                        })
                    })

    @channel_session_user
    def ws_receive(message):
        global l1

        try:
            data = json.loads(message['text'])
        except ValueError:
            log.debug("ws message isn't json text=%s", message['text'])
            return None

        if data:
            reply_channel = message.reply_channel.name
            if data['action'] == "connected":
                l1= ListenTask()
                l1.listen(message, data, reply_channel, flag)

    @channel_session_user
    def ws_disconnect(message):
        global l1, clients
        clients -=1
        print(clients)
        try:
            l1.flag = False
        except NameError:
            pass
        gc.collect()
        Group('users').discard(message.reply_channel)

    @channel_session_user
    def __del__(self):
      print('leave Connected')
      class_name = self.__class__.__name__
