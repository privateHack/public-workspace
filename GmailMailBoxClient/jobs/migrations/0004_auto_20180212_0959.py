# Generated by Django 2.0.2 on 2018-02-12 09:59

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0003_mailbox_celery_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailbox',
            name='date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
