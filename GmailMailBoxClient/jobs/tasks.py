from __future__ import absolute_import
import json
import logging, sys

from example.celery import app
from channels import Channel

import httplib2
import os
import datetime
import asyncio, urllib.request

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from apiclient import errors
import base64
import email
import inspect
from functools import lru_cache

from jobs.models import MailBox as gmb
import celery

import pickle

log = logging.getLogger(__name__)

try:
    import argparse
    flags = tools.argparser.parse_args([])
except ImportError:
    flags = None

@app.task(bind=True, max_retries=None)
def sec3(self, message_index, reply_channel):
    try:
        search = DownloadMessages(message_index, reply_channel)
        search.GetMessage()
        raise self.retry(countdown=15)
    except ConnectionResetError:
        pass

class DownloadMessages():
    def __init__(self, message_index, reply_channel):
        self.message_index = message_index
        self.reply_channel = reply_channel
        self.tasks = None or []
        self.counter_task = 0
        self.count = 0

        http = self.get_credentials().authorize(httplib2.Http())
        self.service = discovery.build('gmail', 'v1', http=http)
        self.user_id = 'me'
        self.label_ids=['CATEGORY_PERSONAL']

    def get_credentials(self):
        SCOPES = 'http://www.googleapis.com/auth/gmail'
        CLIENT_SECRET_FILE = '/home/jarek/Pulpit/Projects/Django/mysite/global_app/api_gmail/backgorund_secret_key/client_secret_gmail.json'
        APPLICATION_NAME = 'Gmail API Python Quickstart'
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,'gmail-python-quickstart.json')

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            flow.user_agent = APPLICATION_NAME
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else:
                credentials = tools.run(flow, store)
        return credentials

    def get_rc_channel(self):
        with open("/tmp/data_rc", "rb") as myFile:
            d = pickle.load(myFile)
            return d

    def GetMessage(self):
        responses = self.service.users().messages().list(userId=self.user_id,
                            labelIds=self.label_ids, maxResults=50).execute()
        mails = None or []

        [mails.append(response) for response in responses['messages'] if 'messages' in responses and not gmb.objects.filter(id_mail=response['id'])]

        if mails != []:
            loop = asyncio.get_event_loop()
            loop.run_until_complete(self.download_async_messages(mails))
            loop.stop()

    async def download_async_messages(self, mails):
            [self.tasks.append(self.before_download(id_mail)) for id_mail in mails]
            self.counter_task = len(self.tasks)
            completed = await asyncio.wait(self.tasks)

    async def counter_percent(self):
            self.count += 1
            sum_percent = float(self.count/len(self.tasks))*100
            Channel(self.reply_channel).send({
                    "text": json.dumps({
                        "value_percent":"{0:.0f}".format(round(sum_percent,0)),
                        "action":'update_progress_bar',
                    })
                })
            if len(self.tasks) == self.count:
                Channel(self.reply_channel).send({
                        "text": json.dumps({
                            "action":'stop_update',
                        })
                    })
                return None

    async def before_download(self, id_mail):
            message = self.service.users().messages().get(userId=self.user_id,
                                    id=id_mail['id'], format='full').execute()
            body = message['snippet']
            id_mail = id_mail['id']
            for x in message['payload']['headers']:
                for k, v in x.items():
                    if v == 'Subject':
                        subject = (x['value'])
                    elif v == 'From':
                        _from = (x['value'])
                    else:
                        break

            await self.post_mail_to_db(_from, subject, body, id_mail)

    async def post_mail_to_db(self, _from=None, header=None, body=None, id_mail=None):
            q = gmb(header=header, body=body, _from=_from, id_mail=id_mail)
            q.save()
            stat = self.get_rc_channel()
            print(stat)
            for k, v in stat.items():
                Channel(v).send({
                        "text": json.dumps({
                            "header": q.header,
                            "body": q.body,
                            "_from": q._from,
                            "date": json.dumps(q.date, default = self.myconverter),
                            "action":'update_mail',
                        })
                    })
            await self.counter_percent()

    def myconverter(self, o):
        if isinstance(o, datetime.datetime):
            return o.__str__()

    def __del__(self):
        self.__class__.__name__
