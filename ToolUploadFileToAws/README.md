# Tool for update files to AWS cloud
Tools designed for asynchronous file transfer to Amazon cloud.

# Requirements
- Python3.6.3 version
- In terminal enter: <br />
    cd ~/.tool_upload_file_to_aws <br />
    pip3 install -r requirements.txt

# Quick Start
- set in config.ini path to folder with upload files and your own name bucket folder <br />
  to upload files.

- In terminal: <br />
  python3 beta_upload_async.py
