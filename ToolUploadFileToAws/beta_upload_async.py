import asyncio
import aiobotocore
import os, configparser

class Config():
    @staticmethod
    def set_config(path='config.ini'):
        path = os.path.join(
                        os.path.dirname(os.path.abspath(__file__)),
                        path
                        )
        config = configparser.ConfigParser()
        config.sections()
        config.read(path)
        return config['DEFAULT'].items()

    @classmethod
    def get_config(cls):
        obj = cls()
        for k, v in Config.set_config():
            setattr(cls, k.lower(), v)
        return obj

async def upload_spec_file(file, client, sema):
    filename = file
    folder = c.own_bucket_folder
    key = '{}/{}'.format(folder, filename)
    with open(os.path.join(c.path_to_files, file), 'rb') as data:
        await client.put_object(ACL='aws-exec-read',
                                            Bucket=c.bucket,
                                            Key=key,
                                            Body=data,
                                            ContentType= 'video/mp4'
                                )

async def bound_fetch(file, sema, client):
    async with sema:
        await upload_spec_file(file, client, sema)

async def go(loop):
    sema = asyncio.BoundedSemaphore(3, loop=loop)
    session = aiobotocore.get_session(loop=loop)
    async with session.create_client(c.bucket_session,
                           aws_secret_access_key=c.aws_secret_access_key,
                           aws_access_key_id=c.aws_access_key_id,
                           use_ssl=False,
                           verify=False) as client:
        status = await asyncio.gather(*[
                        asyncio.ensure_future(bound_fetch(
                                                _, sema, client
                                                ))\
                                    for _ in os.listdir(
                                                os.path.join(c.path_to_files)
                                                )
                                    ]
                    )

c = Config().get_config()
loop = asyncio.get_event_loop()
loop.run_until_complete(go(loop))
loop.close()
